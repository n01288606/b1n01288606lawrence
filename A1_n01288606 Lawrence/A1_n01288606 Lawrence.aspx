﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="A1_n01288606 Lawrence.aspx.cs" Inherits="A1_n01288606_Lawrence.A1_n01288606_Lawrence" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lending Library</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Lending Library: Set your parameters and a mystery item will be selected </p>
            <label> First Name:</label><asp:TextBox runat="server" ID="fname" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter First Name" ControlToValidate="fname" ID="validatorfname"></asp:RequiredFieldValidator>
            <br />
            <br /><label>Last Name:</label><asp:TextBox runat="server" ID="lname" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Last Name" ControlToValidate="lname" ID="validatorlname"></asp:RequiredFieldValidator>
            <br />
            <br /><label>Email:</label><asp:TextBox runat="server" ID="email" placeholder="Email Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Valid Email" ControlToValidate="email" ID="Validatoremail"></asp:RequiredFieldValidator>
           <asp:RegularExpressionValidator ID="EmailValidator"  runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ErrorMessage="Invalid Email"></asp:RegularExpressionValidator>
            <br />
            <br /><label>Mailing Address:</label><asp:TextBox runat="server" ID="Address" placeholder="Mailing Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Mailing Address" ControlToValidate="Address" ID="MailValidator"></asp:RequiredFieldValidator>

            <br />
            <br /><label>Rental Types:</label>
            <asp:RadioButton GroupName="RentalType" runat="server" Text="Video Games" />
            <asp:RadioButton GroupName="RentalType" runat="server" Text="Board Games" />
            <asp:RadioButton GroupName="RentalType" runat="server" Text="Books" /><br />

            <br /> <label>Number of Items:</label>
            <asp:TextBox runat="server" ID="itemnum" placeholder="Choose Amount of Items"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter an Amount" ControlToValidate="itemnum" ID="Itemvalidator"></asp:RequiredFieldValidator><br />
            <asp:RangeValidator runat="server" ControlToValidate="itemnum" Type="Integer" MinimumValue="1" MaximumValue="3" ErrorMessage="Please Choose a Quantity Between 1-3"></asp:RangeValidator>

            <br />
            <br /><label>Age Range:</label>
                <asp:RadioButton GroupName="Age" runat="server" Text="Child (7-12)"></asp:RadioButton>
                <asp:RadioButton GroupName="Age" runat="server" Text="Teen (13-17)"></asp:RadioButton>
                <asp:RadioButton GroupName="Age" runat="server" Text="Mature (18+)"></asp:RadioButton>
            
            
            <div id="genre" runat="server">
                <p>Pick Between 1-8 and Your Item Will Fall Into Any of Those Categories </p>
                <label>Genres:</label> 
                <asp:CheckBox runat="server" ID="genre1" Text="Horror"/>
                <asp:CheckBox runat="server" ID="genre2" Text="Action" />
                <asp:CheckBox runat="server" ID="genre3" Text="Romance" />
                <asp:CheckBox runat="server" ID="genre4" Text="Fantasy" /> <br />
                <asp:CheckBox runat="server" ID="genre5" Text="Mystery" />
                <asp:CheckBox runat="server" ID="genre6" Text="Adventure" />
                <asp:CheckBox runat="server" ID="genre7" Text="Tragedy" />
                <asp:CheckBox runat="server" ID="genre8" Text="Science-Fiction" />

            </div>

           <br / <label>Delivery Times:</label>
            <asp:DropDownList runat="server" ID="times">
                <asp:ListItem Value="t1" Text="11:00AM"></asp:ListItem>
                <asp:ListItem Value="t2" Text="12:00PM"></asp:ListItem>
                <asp:ListItem Value="t3" Text="1:00PM"></asp:ListItem>
                <asp:ListItem Value="t4" Text="2:00PM"></asp:ListItem>
                <asp:ListItem Value="t5" Text="3:00PM"></asp:ListItem>
                <asp:ListItem Value="t6" Text="4:00PM"></asp:ListItem>
                <asp:ListItem Value="t7" Text="5:00PM"></asp:ListItem>
                <asp:ListItem Value="t8" Text="6:00PM"></asp:ListItem>
                <asp:ListItem Value="t9" Text="7:00PM"></asp:ListItem>
                <asp:ListItem Value="t10" Text="8:00PM"></asp:ListItem>
            </asp:DropDownList> 
            <br />
            <br /><label>Have You Used This Service Before?</label> <br />
             <asp:RadioButton GroupName="Rating" runat="server" Text="1-3 Times"/>
             <asp:RadioButton GroupName="Rating" runat="server" Text="3-5 Times" />
             <asp:RadioButton GroupName="Rating" runat="server" Text="5-7 Times" />
             <asp:RadioButton GroupName="Rating" runat="server" Text="7+ Times" />
            <br />
            <br /><label>Where Can We Improve?</label> <br />
                <asp:CheckBox runat="server" ID="Imp1" Text="Expand Genres"/>
                <asp:CheckBox runat="server" ID="Imp2" Text="Delivery Times" />
                <asp:CheckBox runat="server" ID="Imp3" Text="Customer Service" />
                <asp:CheckBox runat="server" ID="Imp4" Text="Other" />
            <asp:TextBox runat="server" ID="other" placeholder="Other"></asp:TextBox> <br />

           <br/><asp:Button Text="Submit" runat="server" /> 
        </div>
        <asp:ValidationSummary runat="server" ID="ValidationSummary" />
    </form>
</body>
</html>
